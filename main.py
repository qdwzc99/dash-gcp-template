# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
flights = pd.read_csv('https://raw.githubusercontent.com/Cuiyingzhe/dataset/main/flights.csv.gz')
fig1=px.histogram(flights,x='distance',color='origin',nbins=45)

fig1.update_layout(
    title={
        'text': "Histogram of flight distance of three airports", 
        'y':0.9,  
        'x':0.5,
        'xanchor': 'center',   
        'yanchor': 'top'},
    xaxis_title="Flights Distance",  
    yaxis_title="Flights Count")

fig2=px.box(flights,x='origin',y='arr_delay',color='origin',range_y=[-100,100])
fig2.update_layout(
    title={
        'text': "Boxplots of flight delay time of three airports", 
        'y':0.9,  
        'x':0.5,
        'xanchor': 'center',   
        'yanchor': 'top'},
    xaxis_title="Departure Airport",  
    yaxis_title="Flight Delay Time")

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Figure 1: Below is the histogram of flight distance of three departure airports.We can see that in general the distance of most flights are within 0-1200 km.
        Flights from JFK fly more on long-distance routes and from LGA on short-distance routes.
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig1
    ),

    html.Div(children='''
       Figure 2:Below are the boxplots of flight delay time of three airports. We notice that the median, upper and lower quantile delay time of three airports are pretty similar.
       But the absolute value of upper and lower fence of EWR are bigger than the other two.All three airports have lots of outliers above 0, which means there are lots of 
       flights delaying for at least 1 hour for all airports.
    '''),

    dcc.Graph(
        id='example-graph2',
        figure=fig2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
